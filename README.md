# teplomer FireBeetle ESP8266
Release Notes 64996:
* data odesilame pomoci zabbix_send, zručeno odesilani pres http
* optimalozovano chovani WiFi  podle (https://arduino.stackexchange.com/questions/45351/how-to-optimise-tx-power-for-esp8266/)


[schenma](https://gitlab.com/pavel.sleis/teplomer-firebeetle-esp8266/-/blob/main/FireBe01.jmn.png)
