#include <ESP8266ZabbixSender.h>
#include <ESP8266WiFi.h>
#include <OneWire.h>
#include <DallasTemperature.h>

// insert user specific configuration
#include "wificonf.h"

#define uS_TO_S_FACTOR 1000000              // prevod mikrosekund na sekundy
#define TIME_TO_SLEEP 720                   // pocet sekund v hlubokem v DeepSleep
#define ONE_WIRE_PIN 12                     // pin pro cteni data teplomeru MISO
#define GPIO_OUT_TEMP 14                    // pin pro napajeni teplomeru SCK
#define GPIO_INDICATOR 2                    // indikator odesilani dat (modra LED na desce)
#define analogInPin A0 
#define R_top 320                           // hodnota horniho odporu delice pro mereni napeti baterky (320 k pro FireBeetle desku)
#define R_bot 100                           // hodnota spodniho odporu delice pro mereni napeti baterky (320 k pro FireBeetle desku)
#define ADC_Umax 1.00F                      // referencni napeti AD prevodniku (1,00 V pro ESP8266)
#define ADC_Nmax 1023                       // rozliseni AD prevodniku (10b pro ESP8266)
#define ADC_V_corr 0.05F                    // korekce nepresnosti ADC, pricita se k vysledku [V]
#define MIN_BATTERY_LEVEL 2.8               // TODO pokud klesne hodnota napeti na tuto hodnotu, FireBee posle morseovkou vzkaz charge batt a nasledne se trvale vypne
#define MAX_ATTEMPT_CONN 9                  // max attempts(+1) pocet pokusu pripojot se k wifi v jednom cyklu
#define DEBUG false                           // seriove vypisy jsou zapnuty pouze pri DEBUG true

// include from wificonf.h
const char* SSID     = HOME_WIFI_SSID;
const char* PASSWORD = HOME_WIFI_PSWD;

int CONN_DELAY = 600;                           // defaultni delay pro opakovane pokusy pripojit se k WiFi
long RUN_TIME = 0;                              // merim delku behu scriptu

ESP8266ZabbixSender zSender;
IPAddress zabbixSRV_ip(ZABBIXSERV);
OneWire oneWire(ONE_WIRE_PIN);
DallasTemperature tempSensor(&oneWire);

void init_temperature(){
  tempSensor.begin();
  tempSensor.setWaitForConversion(true);
}

float get_temperature(){
  tempSensor.begin();
  tempSensor.setWaitForConversion(true);
  tempSensor.requestTemperatures();
  return tempSensor.getTempCByIndex(0);
}

float get_voltage(){
  return ADC_Umax*(float)(analogRead(analogInPin))/ADC_Nmax*(1.0F+R_top/R_bot) + ADC_V_corr;
}

void serial_print_state(){
 Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
  Serial.print("GW address: ");
  Serial.println(WiFi.gatewayIP());
  Serial.print("WiFi signal strength: ");
  Serial.println(WiFi.RSSI());
  Serial.print("DNS: ");
  Serial.println(WiFi.dnsIP());
  Serial.print("MAC address: ");
  Serial.println(WiFi.macAddress());
  Serial.print("WiFi status: ");
  Serial.println(WiFi.status());
  Serial.print("Hostname: ");
  Serial.println(WiFi.hostname());
  Serial.print("WiFi cannel: ");
  Serial.println(WiFi.channel());
  Serial.print("WiFi mode: ");
  Serial.println(WiFi.getMode());
}

void setup() {
  RUN_TIME = micros();                                          // zacni pocite micros
//  WiFi.mode(WIFI_OFF);                                          // vypni wifi
  if (DEBUG) Serial.begin(115200);
  if (DEBUG) Serial.println("\n\n");
  zSender.Init(zabbixSRV_ip, ZABBIXPORT, WiFi.hostname());           // inicializace zabbix 
  zSender.ClearItem();
  
  WiFi.persistent(false);                                       // neukladej si wifi konfiguraci do flash memory (setri to vcas a mV)
  WiFi.mode(WIFI_STA);                                          // nastav mode WiFi na station (klient)
  WiFi.begin(SSID, PASSWORD);                                   // pripoj se k wifi

  zSender.AddItem("napeti", get_voltage());                     // zjistit voltage baterie
  pinMode(GPIO_OUT_TEMP, OUTPUT);                               // zapni napajeni teplomeru
  digitalWrite(GPIO_OUT_TEMP, HIGH);                            // zapni napajeni teplomeru
  zSender.AddItem("teplota", get_temperature());                // nacti telotu z teplomeru
  digitalWrite(GPIO_OUT_TEMP, LOW);                             // vypni napajeni teplomeru
  int concounter = 0;                                           // pocitadlo pokusu o pripojeni k wifi
  delay(CONN_DELAY);
  if (DEBUG) Serial.print("Connecting WiFi: ");
  while((WiFi.status() != WL_CONNECTED) && (concounter < MAX_ATTEMPT_CONN)) {
    if (DEBUG) Serial.print(".");
    concounter++;
    delay(CONN_DELAY);
  }
  
  if(WiFi.status() == WL_CONNECTED) {
    if (DEBUG) Serial.println(" connected");
    zSender.AddItem("signal", WiFi.RSSI());                       // nacti silu wifi signalu  dBm

    pinMode(GPIO_INDICATOR, OUTPUT);                              // rozsvit indikaci
    digitalWrite(GPIO_INDICATOR, HIGH);
    
    //serial_print_state();                                       // vypise spoustu infromaci :)

    if (zSender.Send() == EXIT_FAILURE){                          // odesli data na zabbix server
      if (DEBUG) Serial.println("\[ERROR\] zabbix send data error");
    }
    
    WiFi.disconnect( true );
    delay(1);

    digitalWrite(GPIO_INDICATOR, LOW);                            // zhasni indikaci provozu

  } else if (DEBUG) {
    Serial.println("\nNot connected to WiFi");
  }
 
  // a hajdy na kute
  if (DEBUG){
    Serial.println("Setup ESP6288 to sleep for every " + String(TIME_TO_SLEEP) + " Seconds");
    Serial.println("Going to sleep now");
    Serial.print("Run time: \t\t\t\t ");
    Serial.println(micros()-RUN_TIME);
    Serial.flush(); 
  }
  
  ESP.deepSleep(TIME_TO_SLEEP * uS_TO_S_FACTOR);
}

void loop() {
}
